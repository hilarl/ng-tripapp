/*global $, angular */

(function () {
    "use strict";
}());

/* App Module */

angular.module('hotelServices', ['ngResource']).
    factory('Hotel', function ($resource) {
        
        'use strict';
        
        return $resource('hotels/:hotelId.json', {}, {
            query: {
                method: 'GET',
                params: {
                    hotelId: 'hotels'
                },
                isArray: true
            }
        });
    });