/*global $, angular, console */

(function () {
    "use strict";
}());

angular.module('tripApp.directives', []).

	directive('prevent', function () {
        
        'use strict';
        
        return function (scope, element, attrs) {
            $(element).click(function (event) {
                event.preventDefault();
            });
        };
    })

	.directive('fadeIn', function () {
        
        'use strict';
        
	    return {
            compile: function (elm) {
                console.log('compiling');
                $(elm).css('opacity', 0.1);
                return function (scope, elm, attrs) {
                    console.log('animating');
                    $(elm).animate({ opacity : 1.0 }, 1000);
                };
            }
        };
    });