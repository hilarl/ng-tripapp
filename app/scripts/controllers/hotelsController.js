/*global $, tripApp, google, window, document, Masonry */

(function () {
    "use strict";
}());

tripApp.controller('HotelsController', function ($scope, $timeout, $http, Hotel) {

    'use strict';
    
    $('#login-close').click(function (e) {
        e.preventDefault();
        $('#login-info').fadeOut();
    });

    // destination autocomplete
  	
    $scope.selected = undefined;
    $scope.countries = ['Maldives', 'India'];

  // set window height

    if ($(window).width() > 980) {
        var windowHeight = $(window).height();
        $("#cover-photo").css("height", windowHeight);
    }

    // apply masonry

    $timeout(function () {
        var container = document.querySelector('#tiles'),
            msnry = new Masonry(container, {
            // options
                itemSelector: '.tile'
            });
    }, 2000);

    // Hotels search api call

    $scope.hotelSearchForm = {};
    $scope.searchHotel = function () {
        $http.post('json/hotels.json', $scope.hotelSearchForm).success(function (data, status, headers, config) {
            $scope.hotels = data;
        }).error(function (data, status, headers, config) {
            $scope.status = status;
        });
    };

    // Hotels search filters api call

    $scope.hotelSearchFiltersForm = {};
    $scope.applyHotelFilters = function () {
        $http.post('api/hotels/filters/', $scope.hotelSearchFiltersForm).success(function (data, status, headers, config) {
            $scope.hotels = data;
        }).error(function (data, status, headers, config) {
            $scope.status = status;
        });
    };

    // auto scroll to hotel tiles
    
    if ($(window).width() > 980) {
        $timeout(function () {
            var y = $(window).scrollTop();
            $("html, body").animate({
                scrollTop: y + $(window).height() + 1
            }, 500);
        }, 2000);
    }

    $scope.roomCount = 0;
    $scope.incrementRoomCount = function () {
        $scope.roomCount++;
    };
    $scope.decrementRoomCount = function () {
        $scope.roomCount--;
    };

    $scope.personCount = 0;
    $scope.incrementPersonCount = function () {
        $scope.personCount++;
    };
    $scope.decrementPersonCount = function () {
        $scope.personCount--;
    };
  
    $scope.activateFilterMenu = function () {
        $(".filter-menu").slideToggle(100);
    };

    $scope.checkOutDateController = function ($scope) {
        $scope.showWeeks = false;
    };

});