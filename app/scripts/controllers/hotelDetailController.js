/*global $, tripApp, google */

(function () {
    "use strict";
}());

tripApp.controller('HotelDetailController', function ($scope, $timeout, Hotel, $stateParams) {

    'use strict';
    
	$scope.hotel = Hotel.get({
        hotelId: $stateParams.hotelId
    }, function (hotel) {
        $scope.mainImageUrl = hotel.images[0];
    });

    $scope.hotelId = $stateParams.hotelId;

    $scope.setImage = function (imageUrl) {
        $scope.mainImageUrl = imageUrl;
    };

    $scope.mapOptions = {
        center: new google.maps.LatLng(35.784, -78.670),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
  	
    $('#place-nav a').click(function (e) {
        e.preventDefault();
    });
    
});

tripApp.controller('HotelRoomsController', function ($scope, $timeout, Hotel, $stateParams, $http) {
	
    'use strict';
    
	$scope.hotelId = $stateParams.hotelId;

	$http.post('hotels/' + $scope.hotelId + '/rooms.json', $scope.hotelRoomSearchForm).success(function (data, status, headers, config) {
        $scope.rooms = data;
    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });

});

tripApp.controller('HotelPhotoAlbumController', function ($scope, $timeout, Hotel, $stateParams, $http) {
    
    'use strict';
    
    $scope.hotelId = $stateParams.hotelId;
    $scope.albumId = $stateParams.albumId;

    $http({
        method: 'GET',
        url: 'hotels/' + $scope.hotelId + '/photos/' + $scope.albumId + '.json'
    }).success(function (data, status, headers, config) {
        $scope.photos = data;
    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });

});

tripApp.controller('HotelCalendarController', function ($scope, $timeout, $stateParams) {

    'use strict';
	
});