tripApp.controller('RestaurantsController', function($scope, $timeout, Restaurants) {
	Restaurants.get(function(data){
	$scope.restaurantsMain = data.main;
    $scope.restaurants = data.restaurants;
  });

	if ($(window).width() > 980) {
		var windowHeight = $(window).height();
		$("#cover-photo").css("height", windowHeight);
	}

  	$timeout(function() {
    	var container = document.querySelector('#tiles');
			var msnry = new Masonry( container, {
			// options
			itemSelector: '.tile'
		});             
	}, 1000);
 });