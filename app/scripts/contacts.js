angular
	.module('contacts', [])
	.config(function($routeProvider) {
		$routeProvider
			.when('/contact/:index', {
				templateUrl: 'edit.html',
				controller: 'Edit'
			})
			.when('/contact/add', {
				templateUrl: 'edit.html',
				controller: 'Add'
			})
			.when('/contact/delete:index', {
				templateUrl: 'edit.html',
				controller: 'Delete'
			})
			.when('/', {
				templateUrl: 'list.html'
			});
	})
	.controller('Contacts', function($scope) {
		$scope.contacts = [
			{ id: 0, name: 'Tom', number: '123456789643' },
			{ id: 1, name: 'Jeffrey', number: '736251937352' },
			{ id: 2, name: 'Joe', number: '6452729452528' }
		];
	})
	.controller('Edit', function($scope, $routeParams) {
		$scope.contact = $scope.contacts[$routeParams.index],
		$scope.index = $routeParams.index;
	})
	.controller('Add', function($scope, $routeParams) {
		var length = $scope.contacts.push({
			name: "New Contact",
			number: ""
		});
		$scope.contact = $scope.contacts[length-1];
		$scope.index = length -1;
	})
	.controller('Delete', function($scope, $routeParams, $location) {
		$scope.contacts.splice($routeParams.index, 1);
		$location.path('/').replace();
	});