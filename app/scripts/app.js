/*global $, angular, tripApp, google */

var tripApp = angular.module('tripApp', ['ui.state', 'ui.bootstrap', 'ui.calendar', 'ui.map', 'infinite-scroll', 'tripApp.directives', 'hotelServices', 'ngSanitize']);
tripApp
    .value('$anchorScroll', angular.noop)
    .config(function ($stateProvider, $urlRouterProvider) {

        'use strict';
        
        $urlRouterProvider.otherwise("/hotels");
      
        $stateProvider
            .state('hotels', {
                url: "/hotels",
                templateUrl: "templates/hotels.html",
                controller: "HotelsController"
            })
            .state("hotels.search", {
                url: "/search",
                templateUrl: "templates/hotels.search.html",
                controller: "HotelsController"
            })
            .state("p", {
                url: "/p",
                templateUrl: "templates/subpage.html",
                controller: "HotelDetailController"
            })
            .state("p.hotel", {
                url: "/:hotelId",
                templateUrl: "templates/hotel.html",
                controller: "HotelDetailController"
            })
            .state("p.hotel.index", {
                url: "/",
                templateUrl: "templates/hotel.home.html",
                controller: "HotelDetailController"
            })
        
            // hotel states
        
            .state("p.hotel.options", {
                url: "/options",
                templateUrl: "templates/hotel.options.html",
                controller: "HotelRoomsController"
            })
            .state("p.hotel.reviews", {
                url: "/reviews",
                templateUrl: "templates/hotel.reviews.html",
                controller: "HotelDetailController"
            })
            .state("p.hotel.photos", {
                url: "/photos",
                templateUrl: "templates/hotel.photos.html",
                controller: "HotelDetailController"
            })
            .state("p.hotel.photos.home", {
                url: "/",
                templateUrl: "templates/hotel.photos.home.html",
                controller: "HotelDetailController"
            })
            .state("p.hotel.photos.details", {
                url: "/:albumId",
                templateUrl: "templates/hotel.photos.details.html",
                controller: "HotelPhotoAlbumController"
            })
            .state("p.hotel.calendar", {
                url: "/calendar",
                templateUrl: "templates/hotel.calendar.html",
                controller: "HotelCalendarController"
            })
            .state("p.restaurants", {
                url: "/restaurants",
                templateUrl: "templates/restaurants.html",
                controller: "RestaurantsController"
            });
    });